import React, { useEffect, useState } from 'react';
import Dropdown from '../Dropdown';
import { useDispatch } from 'react-redux';
import { add } from '../../Store/RequestSlice';
import { useSelector } from 'react-redux';
import { motion } from 'framer-motion';
import { useHistory } from 'react-router';

const RequestForm = ({ toggleform }) => {

    const dispatch = useDispatch();

    const [ fromDate, setFromDate ] = useState({day: '', month: '', year: ''});
    const [ tillDate, setTillDate ] = useState({day: '', month: '', year: ''});
    const [ enableButton, setEnableButton ] = useState(false);
    const id = useSelector((state => state.auth.id));
    const [ user ] = useSelector((state) => state.members.list.filter((item) => {
        return item.id == id;
    }));

    const day = 1000 * 3600 * 24;
    const date1 = new Date(`${fromDate.month} ${fromDate.day}, ${fromDate.year} `);
    const date2 = new Date(`${tillDate.month} ${tillDate.day}, ${tillDate.year} `);
    const diffInTime = date1.getTime() - date2.getTime();
    const diffInDays = Math.abs(diffInTime / day);
    const history = useHistory();

    const sendRequest = () => {

        dispatch(add({fromDate, tillDate, id, diffInDays}))
        .then(() => {
            history.push('/success')
        })
    }

    const days = [
        0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31
    ]

    const months = [
        'January',
        'February',
        'March',
        'April', 
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ]

    const years = [
        '2021',
        '2022',
        '2023',
        '2024',
        '2025'
    ]

    useEffect(() => {

        const fromList = Object.values(fromDate);
        const tillList = Object.values(tillDate);

        if(fromList.every(item => item !== '') && tillList.every(item => item !== '') && user.days >= diffInDays){
            setEnableButton(true);
        } 

    }, [fromDate, tillDate])

    const requestVariants = {
        hidden: {
            opacity: 0
        },
        visible: {
            opacity: 1
        },
        exit: {
            opacity: 0
        }
    }

    return(
        <motion.div className='request__form'
            variants={ requestVariants }
            initial='hidden'
            animate='visible'
            exit='exit'
        >
            <div className='request__form__date'>
                <div className='text'>van</div>
                <div className='request__form__date__picker'>
                    <Dropdown date={fromDate} dateList={days} changedate={setFromDate} id={'day'} date={fromDate}/>
                    <Dropdown  date={fromDate} dateList={months} changedate={setFromDate} id={'month'} date={fromDate}/>
                    <Dropdown  date={fromDate} dateList={years} changedate={setFromDate} id={'year'} date={fromDate}/>
                </div>
            </div>
            <div className='request__form__date'>
                <div className='text'>tot</div>
                <div className='request__form__date__picker'>
                    <Dropdown date={tillDate} dateList={days} changedate={setTillDate} id={'day'} date={tillDate}/>
                    <Dropdown  date={tillDate} dateList={months} changedate={setTillDate} id={'month'} date={tillDate}/>
                    <Dropdown  date={tillDate} dateList={years} changedate={setTillDate} id={'year'} date={tillDate}/>
                </div>
            </div>

            <div onClick={ () => sendRequest()} className={`request__btn btn normal ${enableButton ? '' : 'disabled'}`}>
                Verstuur
            </div>

        </motion.div>  
        
    )
}

export default RequestForm;