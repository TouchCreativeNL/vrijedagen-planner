import React, { useState } from 'react';
import UIContainer from '../UIContainer';

/* PACKAGES */

import { CircularProgressbar } from 'react-circular-progressbar';

/* IMAGES*/

import legendaIcon from '../../static/Icons/legenda.png'; 
import plusIcon from '../../static/Icons/plus.png';
import RequestForm from './RequestForm';
import { AnimatePresence, motion } from 'framer-motion';
import Legenda from './Legenda';

const RequestDays = ({ remainingdays, requireddays }) => {
    
    const [ form, setForm ] = useState(false);
    const [ legenda, setLegenda ] = useState(false);

    const remainingDaysBar = (100 / 20) * remainingdays - 5;
    const requiredDaysBar = remainingDaysBar + 5;

    const overviewVariants = {
        exit: {
           opacity: 0
        }
    }

    return (
        <div className='request'>
            
            <UIContainer>

                    <div className='header'>
                        <div className='title title-sub'>
                            <b>{ remainingdays }</b> dagen totaal te besteden
                        </div>
                        <div className='information icon'>
                            <img onClick={ () => setLegenda(true) } src={ legendaIcon } alt='legenda-icon'/>
                        </div>
                    </div>
                    <AnimatePresence exitBeforeEnter>
                        {form ?
                            <RequestForm toggleform={setForm} />
                        :
                            
                            <motion.div key="request-overview" className='request__overview'
                                variants={ overviewVariants }
                                exit="exit"
                            >
                            <div className='request__overview__progression'>
                                <div className='request__overview__progression__circle'>
                                <div className='request__overview__progression__circle--useddays'>
                                        <CircularProgressbar strokeWidth={ 3 } value={100} />  
                                    </div>
                                    <div className='request__overview__progression__circle--remainingdays'>
                                        <CircularProgressbar strokeWidth={ 3 } value={remainingDaysBar} />  
                                    </div>
                                    <div className='request__overview__progression__circle--reserveddays'>
                                        <CircularProgressbar strokeWidth={ 3 } value={requiredDaysBar} />  
                                    </div>
                                </div>
                                <div className='request__overview__progression__text'>
                                    <div className='title title-big'>
                                        { remainingdays }
                                    </div>
                                    <div className='title title-sub'>
                                        dagen resterend
                                    </div>
                                </div>
                            </div>

                            
                        <div className='request__days'>

                            <div onClick={() => setForm(true) } className='btn normal'>

                                <div className='plus icon'>
                                    <img src={ plusIcon } alt='plus-icon'/>
                                </div>
                                <div className='text'>
                                    Vakantie dagen opnemen
                                </div>

                            </div>

                            </div>

                        </motion.div>
                            
                        }
                    </AnimatePresence>
            </UIContainer>
            {legenda && <Legenda setlegenda={setLegenda} />}
        </div>
    )
}

export default RequestDays;