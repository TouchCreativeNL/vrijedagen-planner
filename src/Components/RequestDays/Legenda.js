import React from 'react';
import blueDot from '../../static/Icons/ellipse_blauw.png';
import greyDot from '../../static/Icons/ellipse_grijs.png';
import redDot from '../../static/Icons/ellipse_rood.png';
import blackDot from '../../static/Icons/ellipse_zwart.png';
import closeButton from '../../static/Icons/ignore.png';

const Legenda = ({setlegenda}) => {
    return (
        <div className='legenda'>
            <div className='ui-container'>
                <div className='legenda__close'> 
                    <img onClick={() => setlegenda(false)} src={ closeButton }/>
                </div>
                <div className='legenda__header'>
                    <div className='title title-bg'>
                        Legenda
                    </div>
                    <div className='title-sub'>
                        In de cirkeldiagram worden verschillende dagen met verschillende kleuren aangeduid.
                    </div>
                </div>
                <div className='legenda__body'>
                        <div className='legenda__body__list'>
                           <span> Er zijn drie kleuren: </span>
                            <ul>
                                <li><img src={ blackDot }/> Vebruikte dagen</li>
                                <li><img src={ greyDot }/> Verplichte dagen</li>
                                <li><img src={ blueDot }/> Vrije dagen</li>
                                <li><img src={ redDot }/> Voorgaand jaar dagen</li>
                            </ul>
                        </div>
                    </div>
            </div>
        </div>
    )
}

export default Legenda;