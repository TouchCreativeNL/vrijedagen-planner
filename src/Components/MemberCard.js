import React, { useState } from 'react';
import UIContainer from './UIContainer';
import { useDispatch } from 'react-redux';
import { motion, useCycle } from 'framer-motion';

/* IMAGES */

import arrowIcon from '../static/Icons/arrow_onder.png';
import ignoreIcon from '../static/Icons/ignore.png';
import acceptIcon from '../static/Icons/accept.png';
import { accept, refuse } from '../Store/RequestSlice';

const MemberCard = ({ name, remainingdays, history, id }) => {

    const barFill = (100 / 25) * remainingdays;
    const [ slideDownAnimation, setSlideDownAnimation ] = useCycle('closed', 'open');
    const hasPending = history.some(item => item.status == 'pending');
    const dispatch = useDispatch();

    const memberBodyVariants = {
        initial: {
            height: 0,
            opacity: 0
        },
        open: {
            height: 'auto',
            opacity: 1,
            marginTop: '1rem',
            transition: {
                type: 'tween'
            }
        },
        closed: {
            height: 0,
            opacity: 0,
        }
    }

    const dropdownButtonVariants = {
        initial: {
            rotate: 360
        },
        open: {
            rotate: 180,
            transition: {
                type: 'tween'
            }
        }
    }

    return(
        <div className='member'>
                <UIContainer flow={{ direction: 'horizontal', placement:'between' }}>
                    <div className='member__header'>
                        <div className='title title-medium'>
                            { name }    
                        </div> 
                        <div className='member__content__progress'>
                            <div className='bar'>
                                <div style={{ width: `${barFill}%` }}className='bar__days'>

                                </div>
                            </div>
                            <div className='title title-sub'>
                                <b>{ remainingdays }</b> dagen resterend
                            </div>
                        </div>
                        <motion.div className={`member__header__icon ${hasPending ? '' : 'disabled'}`} onClick={() => setSlideDownAnimation()}
                            variants={ dropdownButtonVariants }
                            initial="initial"
                            animate={ slideDownAnimation }
                        >
                            <img src={ arrowIcon } alt='settings-image'/>
                        </motion.div>
                    </div>
                    <motion.div className='member__body'
                        variants={ memberBodyVariants }
                        animate={ slideDownAnimation }
                        initial="initial"
                    >
                    <div class={ 'history__requests' }>  
                    <div className='history__requests__header'>
                        <div className='title title-medium'>
                            Aanvragen
                        </div>
                    </div>
                    <div className='history__requests__list'>
                        {history.map((item, index) => {
                            if(item.status == 'pending'){

                                return(
                                    <div className='request'>
                                    <div className='request__period'>
                                        <div className='request__period__from'>
                                            { item.fromDate }
                                        </div>
                                        <div class='icon'>
        
                                        </div>
                                        <div className='request__period__till'>
                                            { item.tillDate }
                                        </div>
                                    </div>
                                    <div className='request__update'>
                                        <div className='request__update__accept' onClick={ () => dispatch(accept({requestId: index, userId: id, newRemainingDays: remainingdays - item.days})) }>
                                            <img src={ acceptIcon } alt='accept-request'/>
                                        </div>
                                        <div className='request__update__ignore'>
                                            <img src={ ignoreIcon } alt='ignore-request' onClick={ () => dispatch(refuse({requestId: index, userId: id})) } />
                                        </div>
                                    </div>
                                </div>
                                )
                            }
                        })}
                    </div>
                </div>
                    </motion.div>
                </UIContainer>
        </div>
    )
}

export default MemberCard;