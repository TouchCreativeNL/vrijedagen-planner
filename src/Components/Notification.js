import React, { Children, useEffect } from 'react';
import { motion } from 'framer-motion';
import { useDispatch, useSelector } from 'react-redux';
import { disableNotifications } from '../Store/NotificationSlice';

const notificationVariants = {
    hidden: {
        opacity: 0,
        y: '100vh'
    },
    visible: {
        opacity: 1,
        y: -200,
        transition: {
            type: 'tween',
            duration: 0.75
        }
    },
    exit: {
        opacity: 0,
        y: '100vh'
    }
}

const Notification = () => {

    const { notificationShow, notificationText } = useSelector(state => state.notification);
    
    return(
        <>

        {notificationShow && 

            <motion.div className='notification'
            variants={ notificationVariants }
            initial="hidden"
            animate="visible"
            >

                <div className="notification__message">
                    { notificationText }
                </div>

            </motion.div>

        }

        </>
    )
}

export default Notification;