import React, {  useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { getAuth, signOut } from '@firebase/auth';
import { useDispatch } from 'react-redux';
import { logout } from '../Store/AuthSlice';

/* IMAGE */

import logoutIcon from '../static/Icons/logout.png';

const Navigation = () => {

    const dispatch = useDispatch();
    const auth = getAuth();

    const logoutUser = () => {
        signOut(auth)
        .then(() => {
            console.log('Signed out successfully');
            dispatch(logout());
        })
        .catch((err) => {
            console.log(err.message);
        })
    }

    return (
      <div className='navigation'>
        <div onClick={ () => logoutUser() }  className='logout icon'>
            <img src={ logoutIcon } alt='logout-icon'/>
        </div>
     </div> 
    )
}

export default Navigation;