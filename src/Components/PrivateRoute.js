import React from 'react';
import { Route, Redirect } from 'react-router';
import { useSelector } from 'react-redux';
import Navigation from './Navigation';

const PrivateRoute = ({
    component: Component,
    ...rest
}) => {

    const name = useSelector(state => state.auth.userEmail);
    
    return(
        <>
        <Navigation/>
        <Route {...rest}>
            {name ? <Component/> : <Redirect to='/'/>}
        </Route>
        </>
    )
}

export default PrivateRoute;