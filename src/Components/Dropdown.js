import React, { useState } from 'react';
import { motion, useCycle } from 'framer-motion';

/* IMAGES */

import arrowIcon from '../static/Icons/arrow_onder.png';

const Dropdown = ({ dateList, changedate, id, date }) => {

    const [ selected, setSelected ] = useState();
    const [ slideDropdown, setSlideDropdown ] = useCycle('closed', 'open');

    const GenerateList = () => {

        const triggerUpdate = (item) => {
            setSelected(item);
            changedate({
                ...date,
                [id]: item
            });
 
            setSlideDropdown();
        }

        return dateList.map((item) => {
            if(item != selected){
                return <li onClick={() => triggerUpdate(item)} key={ item } >{ item }</li>
            }
        })
    
    }

    const headDropdown = {
        closed: {
            rotate: 360
        },
        open: {
            rotate: 180
        }
    }

    const bodyDropdown = {
        hidden: {
            opacity: 0,
            height: 0
        },
        open: {
            height: 'auto',
            opacity: 1,
            marginTop: '1rem',
            transition: {
                type: 'tween'
            }
        },
        closed: {
            height: 0,
            opacity: 0,
            display: 'none'
        }
    }

    return(
        <div className='dropdown'>
            <div class='dropdown__header'>
                <div className='dropdown__selected'>
                    { selected }
                </div>
                <motion.div onClick={ () => setSlideDropdown() } class="icon"
                    variants={ headDropdown }
                    animate={ slideDropdown }
                >
                    <img src={ arrowIcon } alt='arrow down'/>
                </motion.div>
            </div>
            <motion.ul className='dropdown__list'
                variants={ bodyDropdown }
                initial='hidden'
                animate={ slideDropdown }
            >
                <GenerateList/>
            </motion.ul>
        </div>
    )
}

export default Dropdown;