import React from 'react';

/**
 * Gray container which is responsible for holding ui components
 * 
 */

const UIContainer = ({ children }) => {

    return (
        <div className="ui-container">
            { children } 
        </div>

    );
}

export default UIContainer;
