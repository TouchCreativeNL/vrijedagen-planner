import React from 'react';
import UIContainer from './UIContainer';
import { motion, useCycle } from 'framer-motion';

/* IMAGES */

import arrowIcon from '../static/Icons/arrow_onder.png';

const HistoryCard = ({ history, remainingdays }) => {

    const [ slideDownAnimation, setSlideDownAnimation ] = useCycle('closed', 'open');
    const barFill = (100 / 25) * remainingdays;

    const memberBodyVariants = {
        initial: {
            height: 0,
            opacity: 0
        },
        open: {
            height: 'auto',
            opacity: 1,
            marginTop: '1rem',
            transition: {
                type: 'tween'
            }
        },
        closed: {
            height: 0,
            opacity: 0,
        }
    }

    const dropdownButtonVariants = {
        initial: {
            rotate: 360
        },
        open: {
            rotate: 180,
            transition: {
                type: 'tween'
            }
        }
    }

    return(
        <div className='history'>
            <UIContainer>
                <div className='history__header'>
                    <div className='title title-medium'>
                        2020    
                    </div> 
                    <div className='member__content__progress'>
                        <div className='bar'>
                            <div style={{ width: `${barFill}%` }}  className='bar__days'></div>
                        </div>
                        <div className='title title-sub'>
                            <b>{ remainingdays }</b> dagen resterend
                        </div>
                    </div>
                    <motion.div className='member__content__icon'  onClick={() => setSlideDownAnimation()}
                        variants={ dropdownButtonVariants }
                        initial="initial"
                        animate={ slideDownAnimation }
                    >
                        <img alt='' src={ arrowIcon }/>
                    </motion.div>
                </div>
                <motion.div class='history__request'
                    variants={ memberBodyVariants }
                    animate={ slideDownAnimation }
                    initial="initial"
                >  
                    <div className='history__requests__header'>
                        <div className='title title-medium'>
                            Geschiedenis
                        </div>
                    </div>
                    <div className='history__requests__list'>
                        {history.map((item) => {

                            return(
                                <div className='request'>
                                    <div className='request__status'>
                                        { item.status }
                                    </div>
                                    <div className='request__body'>
                                        <div className='request__body__period'>
                                            <div className='request__body__period__from'>
                                                { item.fromDate }
                                            </div>
                                            <div class='icon'>
            
                                            </div>
                                            <div className='request__body__period__till'>
                                                { item.tillDate }
                                            </div>
                                        </div>
                                        <div className='request__body__days below-average'>
                                            -{ item.days }
                                        </div>
                                    </div>
                            </div>
                            )
                        })}
                    </div>
                </motion.div>
            </UIContainer>
        </div>
    )
}

export default HistoryCard;