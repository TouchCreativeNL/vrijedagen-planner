import React, { useEffect } from 'react';

/* COMPONENTS */

import HistoryCard from '../Components/HistoryCard';

import 'react-circular-progressbar/dist/styles.css';
import RequestDays from '../Components/RequestDays/RequestDays';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { getMemberById } from '../Store/MemberSlice';
import { motion } from 'framer-motion';

const Member = () => {

    const date = new Date();
    const year = date.getFullYear();
    const dispatch = useDispatch();
    const id = useSelector((state => state.auth.id))
    const [ user ] = useSelector((state) => state.members.list.filter((item) => {
        return item.id == id;
    })); 

    useEffect(() => {
        dispatch(getMemberById( id ));
    },[id]);

    console.log(user);

    const containerVariants = {
        hidden: {
            opacity: 0,
            x: '100vw'
        },
        visible: {
            opacity: 1,
            x: 0,
            transition: {
                duration: 0.75,
            }
        },
        exit: {
            x: '-100vw',
            transition: {
                type: 'tween',
                duration: 0.5
            }
        }

    }
    
    return (
        <motion.div className='member'
            variants={ containerVariants }
            initial="hidden"
            animate="visible"
            exit="exit"

        >
            {user ? 
            <>
            <div className='member__header header'>
                <div className='member__header__content'>
                    <div className='title title-big'>
                       { user.name }
                    </div>
                    <div className='title title-sub'>
                        Je vakantiedagen overzicht { year }
                    </div>
                </div>
            </div>

            <div className='member__request'>
                <RequestDays remainingdays={ user.days } requireddays={ user.requiredDays } previousdays={ user.previousDays } />
            </div> 

            {user.history.length > 0 ? 
                <div className='member__history'>
                    <HistoryCard id history={ user.history } remainingdays={ user.days } />
                </div> 
            : ''}
            </>
            : '' }
        </motion.div>
    )
}

export default Member;