import React, { useState } from 'react';

import { useHistory } from 'react-router'

import { Link } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';

import { AnimatePresence, motion } from 'framer-motion';

import Notification from '../Components/Notification';
import UIContainer from '../Components/UIContainer';

import { login } from '../Store/AuthSlice';

import acceptIcon from '../static/Icons/accept-circle.png';

const Login = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');

    const loginStatus = useSelector(state => state.auth.status);

    const containerVariants = {
        hidden: {
            opacity: 0
        },
        visible: {
            opacity: 1,
            transition: {
                duration: 0.75,
            }
        },
        exit: {
            x: '-100vw',
            transition: {
                type: 'tween',
                duration: 0.5
            }
        }

    }

    const buttonVariants = {
        rejected: {
            background: '#131313'
        },
        pending: {
            background: '#edaf00',
            transition: {
                type: 'tween',
                width: {duration: 0.5}
            },
        },
        fulfilled: {
            width: 50,
            background: '#2c7262',
            transition: {
                type: 'tween',
                duration: 5,
                width: { duration: 0.75 },
                background: { duration: 0.5 }
            }
        },

    }

    const dotVariants = {
        initial: { y: 0 },
        visible: i => ({
            y: -5,
            transition: {
                delay: i * 0.3,
                yoyo: Infinity
            }
        }),
    }

    
    const authenticateLogin = () => {

        /**
         *  Call login action 
         *  Login action checks credentials
         *  Based on outcome redirect user to home screen
         *  if credentials do not match the action automatically dispatches a error popup.
         */

        dispatch(login({emailInput: email, passwordInput: password}))
        .unwrap()
        .then(() => {
            history.push('/home');
        });
    }

    return(

        <motion.div className='login'
            variants={ containerVariants }
            initial="hidden"
            animate="visible"
            exit="exit"
            >

            <UIContainer>

                <form className='login__form'>

                    <div className='login__form__input'>

                       <input 
                            value={ email } 
                            onChange={(e) => setEmail(e.target.value)}
                            className='input' 
                            type='text'
                        />

                    </div>

                    <div className='login__form__input'>

                        <input 
                            value={ password } 
                            onChange={(e) => setPassword(e.target.value)} 
                            className='input' 
                            type='password'
                            pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                        /> 

                    </div>

                    <div className='login__form__footer'>
                        
                            <motion.div 
                                key='loginbtn' 
                                onClick={ () => authenticateLogin() } 
                                className='btn btn-normal'
                                variants={ buttonVariants }
                                animate={ loginStatus }
                                exit="fulfilled"
                                >

                                {loginStatus == '' || loginStatus == 'rejected' ? <span>Login</span> : ''}

                                { loginStatus == 'pending' && 
                    
                                <AnimatePresence exitBeforeEnter >

                                    <motion.div key="pending" className='pending' initial={{ y: -100 }} animate={{ y:0 }} exit={{ y: 100 }} >
                                        <motion.div className="pending__dot" custom={1} variants={ dotVariants } initial='initial' animate='visible'></motion.div>
                                        <motion.div className="pending__dot" custom={2} variants={ dotVariants } initial='initial' animate='visible'></motion.div>
                                        <motion.div className="pending__dot" custom={3} variants={ dotVariants } initial='initial' animate='visible'></motion.div>
                                    </motion.div>

                                </AnimatePresence>

                                }

                                { loginStatus == 'fulfilled' &&
                                    <div className='accept-icon icon'>
                                        <img src={ acceptIcon } alt='accept-icon'/>
                                    </div>
                                }

                            </motion.div>
                    </div>

                    <Link to='/register'> no account register here </Link>

                </form>

            </UIContainer>

            <Notification message='Inloggen is niet gelukt' type='error'/>

        </motion.div>
    )
}

export default Login;