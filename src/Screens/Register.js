import React, { useState } from 'react';
import UIContainer from '../Components/UIContainer';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { register } from '../Store/AuthSlice';

const Register = () => {

    const dispatch = useDispatch();
    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');
    const [ name, setName ] = useState('');
    const history = useHistory();

    const authenticateRegister = () => {
        dispatch(register({email: email, name: name, password: password}))
        .then(() => {
            history.push('/');
        })
    }

    return(
        <div className='login'>
            <UIContainer>
                <form className='login__form'>
                    <div className='login__form__input'>
                       <input placeholder='email' value={ email } onChange={(e) => setEmail(e.target.value)}className='input' type='text'/>
                    </div>
                    <div className='login__form__input'>
                       <input placeholder='name' value={ name } onChange={(e) => setName(e.target.value)}className='input' type='text'/>
                    </div>
                    <div className='login__form__input'>
                        <input placeholder='password' value={ password } onChange={(e) => setPassword(e.target.value)} className='input' type='password'/> 
                    </div>

                    <div onClick={ () => authenticateRegister() } className='btn btn-normal'>
                        Register
                    </div>
                </form>
            </UIContainer>
        </div>
    )
}

export default Register;
