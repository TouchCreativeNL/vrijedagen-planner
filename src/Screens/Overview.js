import React, { useEffect } from 'react';
import MemberCard from '../Components/MemberCard';
import { useSelector } from 'react-redux';
import { getMembers } from '../Store/MemberSlice';
import { useDispatch } from 'react-redux';
import { motion } from 'framer-motion';

const Overview = () => {

    const date = new Date();
    const year = date.getFullYear();
    const dispatch = useDispatch();
    const list = useSelector(state => state.members.list);
    
    useEffect(() => {
        dispatch(getMembers());
    },[])

    const containerVariants = {
        hidden: {
            opacity: 0,
            x: '100vw'
        },
        visible: {
            opacity: 1,
            x: 0,
            transition: {
                duration: 0.75,
            }
        },
        exit: {
          opacity: 0,
            transition: {
                duration: 2
            }
        }

    }

    return (
        <motion.div className='members-overview'
            variants={ containerVariants }
            initial="hidden"
            animate="visible"
            exit="exit"
        >
            <div className='header'>
                <div className='title title-big'>
                    Vakantiedagen
                </div>
                <div className='title title-sub'>
                    Overzicht { year }
                </div>
            </div>
            <div className='ui-list'>
                { list.map(({name, days, admin, history, id }) => {
                   if(!admin){
                        return <MemberCard key={ id } history={ history } name={ name } remainingdays={ days } id={ id } />
                    }
                }) }
            </div>
        </motion.div>
    )
}

export default Overview;