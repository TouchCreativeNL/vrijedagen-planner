import React from 'react';
import UIContainer from '../Components/UIContainer';
import { Link } from 'react-router-dom';
import { motion } from 'framer-motion';

const Succes = () => {

    const containerVariants = {
        hidden: {
            opacity: 0
        },
        visible: {
            opacity: 1,
            transition: {
                duration: 0.75,
            }
        },
        exit: {
            x: '-100vw',
            transition: {
                type: 'tween',
                duration: 0.5
            }
        }

    }

    return(

        <motion.div className='succes'
            variants={ containerVariants }
            initial="hidden"
            animate="visible"
            exit="exit"
        >
            <UIContainer>
                <p>
                    Je aanvraag is verwerkt. Zodra je aanvraag is goedgekeurd ontvang je hiervan bericht. De resterende dagen zijn wel al afgetrokken van je totaal aantal vakantiedagen over 2020/2019.
                </p>

                <Link to='/home' className='btn btn-normal'>
                    Terug naar mijn overzicht
                </Link>

            </UIContainer>
        </motion.div>
        
    )

}

export default Succes;