import React from 'react';
import {
    Switch,
    Route,
    useLocation
} from 'react-router-dom';
import { useSelector } from 'react-redux';
import { AnimatePresence } from 'framer-motion';

/* SCREENS */
import Overview from './Screens/Overview';
import Member from './Screens/Member';
import Login from './Screens/Login';
import PrivateRoute from './Components/PrivateRoute';
import Register from './Screens/Register';
import Succes from './Screens/Succes';

const AppRouter = () => {

    const admin = useSelector(state => state.auth.admin);
    const location = useLocation();
    return (
            <AnimatePresence exitBeforeEnter>
                <Switch location={ location } key={ location.key } >
                    <Route exact component={Login} path='/' />
                    <Route exact component={Register} path='/register'/>
                    { 
                    admin ? 
                        <PrivateRoute path='/home' component={Overview}/>
                    :
                        <PrivateRoute path='/home' component={Member}/>
                    }
                    <PrivateRoute path='/success' component={Succes}/>
                </Switch>
            </AnimatePresence>
    )
}

export default AppRouter;