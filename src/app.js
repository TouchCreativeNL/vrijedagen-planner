import React from 'react';
import './scss/main.scss';
import { Container } from '@material-ui/core';
import AppRouter from './AppRouter';
import Legenda from './Components/RequestDays/Legenda';

const App = () => {
    return (
       <div className='vrijedagen-app page'>
           <Container maxWidth='sm'>
                <div class='content'>
                   <AppRouter/>
                </div>
           </Container> 
       </div>
    );
}
 
export default App;