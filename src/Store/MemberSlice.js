import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { collection, doc, getDocs, getDoc } from "firebase/firestore";
import { db } from '../firebase';

const initialState = {
    list: [],
    status: ''
}

export const getMembers = createAsyncThunk(
    'members/getmembers',
    async () => {
        const list = [];
        const querySnapshot = await getDocs(collection(db, 'users'));
        querySnapshot.forEach((doc) => {
            list.push(doc.data());
        })

        return list;

    }
)

export const getMemberById = createAsyncThunk(
    'members/getmember',
    async ( id ) => {
        const docRef = doc(db, "users", id);
        const docSnap = await getDoc(docRef);
        if(docSnap.exists()){
            return docSnap.data();
        } else {
            console.log('Error')
        }
    }
)

const memberSlice = createSlice({
    name: 'members',
    initialState,
    extraReducers: {
        [getMembers.pending]: (state) => {
            state.status = 'pending'
        },
        [getMembers.fulfilled]: (state, { payload }) => {
            state.list = [...payload];
        },
        [getMemberById.pending]: (state) => {
            state.status = 'pending'
        },
        [getMemberById.fulfilled]: (state, { payload }) => {
            state.list = [{ ...payload }]
        } 
    }
})

export default memberSlice.reducer;