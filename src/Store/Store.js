import { configureStore } from "@reduxjs/toolkit";
import memberReducer from './MemberSlice';
import authReducer from './AuthSlice';
import requestReducer from './RequestSlice';
import notificationReducer from './NotificationSlice';

export const Store = configureStore({
    reducer: {
        members: memberReducer,
        auth: authReducer,
        request: requestReducer,
        notification: notificationReducer
    }
});