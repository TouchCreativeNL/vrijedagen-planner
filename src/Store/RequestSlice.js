import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { arrayUnion, doc, getDoc, setDoc, updateDoc } from "@firebase/firestore";
import { db } from "../firebase";
import { getMemberById, getMembers } from "./MemberSlice";

const initialState = {
    status: ''
};

export const add = createAsyncThunk(
    'request/add',
    async ({fromDate, tillDate, id, diffInDays }, { dispatch }) => {
        const docRef = doc(db, 'users', id);
        await updateDoc(docRef, {
            history: arrayUnion({fromDate: Object.values(fromDate).join('-'), tillDate: Object.values(tillDate).join('-'), status: 'pending', days: diffInDays}),
        });
        dispatch(getMemberById( id ));
    }
)

export const accept = createAsyncThunk(
    'request/accept',
    async({ requestId, userId, newRemainingDays }, { dispatch }) => {
        const docRef = doc(db, 'users', userId);
        await getDoc(docRef)
        .then((data) => {
            const { history } = data.data();
            history[requestId].status = 'fulfilled';
            updateDoc(docRef, {
                history: history,
                days: newRemainingDays
            })
            dispatch(getMembers());
        })
        .catch((err) => {
            console.log(err);
        })
    }   
)

export const refuse = createAsyncThunk(
    'request/accept',
    async({ requestId, userId }) => {
        const docRef = doc(db, 'users', userId);
        await getDoc(docRef)
        .then((data) => {
            const { history } = data.data();
            history[requestId].status = 'refuse';
            updateDoc(docRef, {
                history: history
            })
        })
        .catch((err) => {
            console.log(err);
        })
    }   
)

const requestSlice = createSlice({
    name: 'Request',
    initialState,
    extraReducers: {
        [add.pending]: (state) => {
            state.status = 'pending';
        },
        [add.fulfilled]: (state) => {
            state.status = 'fulfilled';
        },
        [accept.pending]: (state) => {
            state.status = 'pending';
        },
        [accept.fulfilled]: (state) => {
            state.status = 'fulfilled';
        },
        [refuse.pending]: (state) => {
            state.status = 'pending';
        },
        [refuse.fulfilled]: (state) => {
            state.status = 'fulfilled';
        }
    }
})

export default requestSlice.reducer;