import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    notificationShow: false,
    notificationText: ''
}

const notificationSlice = createSlice({
    name: 'notification',
    initialState,
    reducers: {
       enableNotification: (state, { payload }) => {
            state.notificationText = payload.text;
            state.notificationShow = true;
       },
       disableNotification: (state) => {
           state.notificationShow = false;
       }
    }
});

export const { enableNotification, disableNotifications } = notificationSlice.actions;
export default notificationSlice.reducer;