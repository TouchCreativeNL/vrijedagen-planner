import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword } from "@firebase/auth";
import { setDoc, doc, getDoc } from "firebase/firestore";
import { db } from "../firebase";
import { enableNotification } from "./NotificationSlice";

const initialState = {
        userEmail: '',
        status: '',
        id: '',
        admin: '',
    }

const auth = getAuth();

export const login = createAsyncThunk(
    'auth/login',
     async ({ emailInput, passwordInput }, thunkAPI) => {
         try {
            const {
                user: {
                    email, uid
                }
            } = await signInWithEmailAndPassword(auth, emailInput, passwordInput);
            thunkAPI.dispatch(update(uid));
            return { email, uid }

         } catch {
            thunkAPI.dispatch(enableNotification({text: 'Kan niet inloggen' }));
            return Promise.reject('Failed to check credentials');
         }
     }
)

export const update = createAsyncThunk(
    'auth/update',
    async(id) => {
        const docRef = doc(db, "users", id);
        const docSnap = await getDoc(docRef);
        if(docSnap.exists()){
            return docSnap.data();
        } else {
            console.log('Error')
        }
    }
)

export const register = createAsyncThunk(
    'auth/register',
    async({email, password, name}) => {
        return createUserWithEmailAndPassword(auth, email, password)
            .then((UserCredentials) => {

                setDoc(doc(db, 'users', UserCredentials.user.uid), {
                    id: UserCredentials.user.uid,
                    email: UserCredentials.user.email,
                    name: name,
                    days: 20,
                    requiredDays: 5,
                    previousDays: 0,
                    admin: false,
                    history: [],
                });

                return UserCredentials.user.email;

            }) 
            .catch((error) => {
                console.log(error.message);
            });
        }
    )

const userSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
      logout: (state) => {
        state.userEmail = '';
        state.status = '';
      }
    },
    extraReducers: {
        [login.pending]: (state) => {
            state.status =  'pending';
        },
        [login.rejected]: (state) => {
            state.status = 'rejected';
        },
        [login.fulfilled]: (state, {payload}) => {
            state.userEmail = payload.email;
            state.id = payload.uid;
            state.status = 'fulfilled';
        },
        [register.pending]: (state) => {
        
        },
        [register.fulfilled]: (state) => {
            
        },
        [update.pending]: (state) => {
            state.status = 'pending'
        },
        [update.fulfilled]: (state, { payload }) => {
            state.status = 'fulfilled';
            state.admin = payload.admin;
        }
    },
})

export const { logout } = userSlice.actions;

export default userSlice.reducer;