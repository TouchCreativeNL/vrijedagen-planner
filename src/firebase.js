// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDJch0LuH9spMvL0PTrE_83l4saiXD--bI",
  authDomain: "vrijedagen-134b4.firebaseapp.com",
  projectId: "vrijedagen-134b4",
  storageBucket: "vrijedagen-134b4.appspot.com",
  messagingSenderId: "538087275456",
  appId: "1:538087275456:web:d45756a11debf66ba3e625"
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);

const db = getFirestore();

export { db };

export default firebaseApp; 
