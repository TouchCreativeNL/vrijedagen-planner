import React from 'react';
import ReactDOM from 'react-dom';
import { firebaseApp } from './firebase';
import App from './app';
import { Provider } from 'react-redux';
import { Store } from './Store/Store';
import { BrowserRouter as Router } from 'react-router-dom';

const Main = () => {
    return (
        <Router>
            <App/>
        </Router>
    )
}

ReactDOM.render(<Provider store={ Store } ><Main/></Provider>, document.querySelector('#root'));